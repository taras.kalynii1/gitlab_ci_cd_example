"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../src/utils");
describe('getTimestamp', () => {
    it('result type should be number', () => {
        const result = (0, utils_1.getTimestamp)();
        expect(typeof result).toEqual('number');
    });
});
